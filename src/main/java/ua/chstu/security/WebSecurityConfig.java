package ua.chstu.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;



@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService service;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                authorizeRequests()
                //.anyRequest().authenticated() Just for Admin
                .antMatchers("/watch/").hasRole("USER")
                .antMatchers("/profile").hasRole("USER")
                .antMatchers("/watch/new").hasRole("ADMIN")
                .antMatchers("/profile").hasRole("ADMIN")
                .antMatchers("/watch/").hasRole("ADMIN")
                .antMatchers("/manager").hasRole("ADMIN")

//                .antMatchers("/manager").permitAll()
                .and()
                .formLogin()
                .loginPage("/signin")
                .usernameParameter("name")
                .passwordParameter("password")
                .permitAll()
                .and()
                .logout().logoutSuccessUrl("/signin?logout")
                .and()
                .csrf().disable();
    }
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(service);
    }
}
