package ua.chstu.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.chstu.dao.User;
import ua.chstu.dao.repository.UserRepository;
import ua.chstu.dao.repository.UserRoleRepository;


@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository repository;
    @Autowired
    private UserRoleRepository roleRepository;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        System.out.println("[LOADED]" + s);
        User user = repository.findByName(s);

        if(user ==null){
            System.out.println("[USER NULL]");
            throw  new UsernameNotFoundException("User " +s);
        }else{
            System.out.println("[LOADED]");
            String role = roleRepository.findRoleByUserName(user.getName());
            return new UserSecurity(user,role);
        }
    }
}
