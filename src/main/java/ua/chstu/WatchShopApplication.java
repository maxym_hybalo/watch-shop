package ua.chstu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class WatchShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(WatchShopApplication.class, args);
	}

}
