package ua.chstu.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name="special")
public class SpecialValue extends BaseEntity{

    @Column(name="value",unique = true)
    private String value;

    public SpecialValue(String value) {
        this.value = value;
    }

    public SpecialValue() {
    }
}
