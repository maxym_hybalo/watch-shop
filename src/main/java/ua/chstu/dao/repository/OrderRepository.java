package ua.chstu.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.chstu.dao.Order;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order,Long>{
    List<Order> findByUserName(String name);
}
