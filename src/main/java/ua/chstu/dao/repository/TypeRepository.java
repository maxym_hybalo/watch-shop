package ua.chstu.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.chstu.dao.Type;

import org.springframework.stereotype.Repository;
@Repository
public interface TypeRepository extends CrudRepository<Type,Long>{

}
