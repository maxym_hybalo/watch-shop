package ua.chstu.dao.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.chstu.dao.Watch;

import java.util.List;

@Repository
public interface WatchRepository extends CrudRepository<Watch,Long>{
    List<Watch> findAll();
    Page<Watch> findAll(Pageable pageable);

    List<Watch> findByPriceBetween(Double from, Double to);


}
