package ua.chstu.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.chstu.dao.Order;
import ua.chstu.dao.User;

import java.util.Set;

public interface UserRepository extends CrudRepository<User,Long>{
    User findByName(String name);
}
