package ua.chstu.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.chstu.dao.Gender;
import org.springframework.stereotype.Repository;

@Repository
public interface GenderRepository extends CrudRepository<Gender,Long>{
}
