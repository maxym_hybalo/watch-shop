package ua.chstu.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.chstu.dao.SpecialValue;

import org.springframework.stereotype.Repository;

@Repository
public interface SpecialRepository extends CrudRepository<SpecialValue,Long>{

}
