package ua.chstu.dao.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.chstu.dao.UserRole;

import java.util.List;
@Repository
public interface UserRoleRepository extends CrudRepository<UserRole,Long>{
    @Query("select a.role from ua.chstu.dao.UserRole a, ua.chstu.dao.User b where b.name=?1 and b.id = a.userId")
    String findRoleByUserName(String name);
}
