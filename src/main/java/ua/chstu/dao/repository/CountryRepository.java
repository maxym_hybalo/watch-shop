package ua.chstu.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.chstu.dao.Country;

import org.springframework.stereotype.Repository;
@Repository
public interface CountryRepository extends CrudRepository<Country,Long>{
}
