package ua.chstu.dao.repository;


import org.springframework.data.repository.CrudRepository;
import ua.chstu.dao.Brand;
import org.springframework.stereotype.Repository;
@Repository
public interface BrandRepository extends CrudRepository<Brand,Long>{
}
