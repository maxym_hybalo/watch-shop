package ua.chstu.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.chstu.dao.Mechanism;
import org.springframework.stereotype.Repository;

@Repository
public interface MechanismRepository extends CrudRepository<Mechanism,Long>{
}
