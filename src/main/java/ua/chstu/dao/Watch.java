package ua.chstu.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name="watch")
public class Watch {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "watch_id")
    Long watchId;

    @Column(name="model")
    private String model;

    @Column(name="price")
    private Double price;

    @Column(name="image")
    private String imagePath;

    @ManyToOne
    private Brand brand;

    @ManyToOne
    private Country country;

    @ManyToOne
    private Gender gender;

    @ManyToOne
    private Mechanism mechanism;

    @ManyToOne
    private SpecialValue specialValue;

    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "watches")
    private Set<Order> orders;

    @ManyToOne
    private Type type;


    public Watch() {
    }

}
