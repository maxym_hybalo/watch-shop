package ua.chstu.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name="mechanism")
public class Mechanism extends BaseEntity {

    @Column(name="type", unique = true)
    private String type;

    public Mechanism(String type) {
        this.type = type;
    }

    public Mechanism() {
    }
}
