package ua.chstu.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name="brand")
public class Brand extends BaseEntity{
    @Column(name="name", nullable = false)
    private String name;

    @Column(name = "image_path", unique = true)
    private String imagePath;

    public Brand(String name) {
        this.name = name;
    }

    public Brand() {
    }
}
