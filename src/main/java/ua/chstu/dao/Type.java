package ua.chstu.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name="type")
public class Type extends BaseEntity{
    @Column(name="name",nullable = false, unique = true)
    private String name;

    public Type(String name) {
        this.name = name;
    }

    public Type() {
    }
}
