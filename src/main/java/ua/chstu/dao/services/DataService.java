package ua.chstu.dao.services;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface DataService {
    void add(Integer i,String ... attr);
    void setFile(MultipartFile file);
    List<?> getAll(Integer table);
}
