package ua.chstu.dao.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import ua.chstu.controller.dto.FilterForm;
import ua.chstu.controller.dto.WatchForm;
import ua.chstu.dao.Watch;

import java.util.List;

public interface WatchService {

    void add(WatchForm watchForm, MultipartFile file,String fileName);

    List<Watch> get(Pageable pageable);

    List<Watch> filter(FilterForm filter);

    Integer getElements();
}
