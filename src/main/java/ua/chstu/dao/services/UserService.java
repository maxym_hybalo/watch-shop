package ua.chstu.dao.services;

import ua.chstu.controller.dto.UserForm;
import ua.chstu.dao.Order;
import ua.chstu.dao.User;

import java.util.List;

public interface UserService {
    void save(UserForm u);
    String getRole(String name);
    List<Order> history(String name);
}
