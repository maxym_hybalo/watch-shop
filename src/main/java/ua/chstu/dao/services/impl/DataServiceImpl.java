package ua.chstu.dao.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ua.chstu.dao.*;
import ua.chstu.dao.repository.*;
import ua.chstu.dao.services.DataService;
import ua.chstu.util.FileLoader;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class DataServiceImpl implements DataService {
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private MechanismRepository mechanismRepository;
    @Autowired
    private GenderRepository genderRepository;
    @Autowired
    private TypeRepository typeRepository;
    @Autowired
    private SpecialRepository specialRepository;
    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private FileLoader loader;

    private MultipartFile file;
    @Override
    public void add(Integer i, String ... attr) {
        switch (i){
            case 2:
                countryRepository.save(new Country(attr[0]));
                break;
            case 4:
                mechanismRepository.save(new Mechanism(attr[0]));
                break;
            case 3:
                genderRepository.save(new Gender(attr[0]));
                break;
            case 5:
                typeRepository.save(new Type(attr[0]));
                break;
            case 6:
                specialRepository.save(new SpecialValue(attr[0]));
                break;
            case 1:
                Brand brand = new Brand(attr[0]);
                brand.setImagePath(saveFile(attr[0],file,attr[1]));
                brandRepository.save(brand);
            default:
                break;
        }
    }

    @Override
    public void setFile(MultipartFile file) {
        this.file = file;
    }
    private String saveFile(String name,MultipartFile file,String fileName){
            loader.writeFile(file,name,fileName);
            return loader.getServerFilePath();
    }
    @Override
    public List<?> getAll(Integer table) {
        List<?> resultCollection = new ArrayList<>();
        switch (table){
            case 1:
                resultCollection.addAll((List)brandRepository.findAll());
                break;
            case 2:
                resultCollection.addAll((List)countryRepository.findAll());
                break;
            case 3:
                resultCollection.addAll((List)genderRepository.findAll());
                break;
            case 4:
                resultCollection.addAll((List)typeRepository.findAll());
                break;
            case 5:
                resultCollection.addAll((List)mechanismRepository.findAll());
                break;
            case 6:
                resultCollection.addAll((List)specialRepository.findAll());
                break;
            default:
                    break;
        }
        return resultCollection;
    }
}
