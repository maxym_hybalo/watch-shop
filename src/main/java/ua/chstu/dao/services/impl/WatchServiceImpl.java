package ua.chstu.dao.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ua.chstu.controller.dto.FilterForm;
import ua.chstu.controller.dto.WatchForm;
import ua.chstu.dao.Watch;
import ua.chstu.dao.repository.*;
import ua.chstu.dao.services.WatchService;
import ua.chstu.util.FileLoader;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Transactional
@Service
public class WatchServiceImpl implements WatchService{

    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private MechanismRepository mechanismRepository;
    @Autowired
    private GenderRepository genderRepository;
    @Autowired
    private TypeRepository typeRepository;
    @Autowired
    private SpecialRepository specialRepository;
    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private WatchRepository watchRepository;

    @Autowired
    private FileLoader loader;

    private int pages;

    @Override
    public void add(WatchForm watchForm, MultipartFile file,String fileName) {

        Watch watch = new Watch();
        watch.setModel(watchForm.getModel());
        watch.setPrice(watchForm.getPrice());
        watch.setBrand(brandRepository.findOne(watchForm.getBrand()));
        watch.setCountry(countryRepository.findOne(watchForm.getCountry()));
        watch.setMechanism(mechanismRepository.findOne(watchForm.getMechanism()));
        watch.setSpecialValue(specialRepository.findOne(watchForm.getJoin()));
        watch.setGender(genderRepository.findOne(watchForm.getGender()));
        watch.setType(typeRepository.findOne(watchForm.getType()));
        watch.setImagePath(saveFile(watchForm.getModel(),file,fileName));
        watchRepository.save(watch);

    }

    @Override
    public List<Watch> get(Pageable pageable) {
        Page<Watch> content = watchRepository.findAll(pageable);
        pages = content.getTotalPages();

        return content.getContent();
    }

    @Override
    public List<Watch> filter(FilterForm filter) {
        List<Watch> watches = watchRepository.findAll();
        List<Watch> result = priceFilter(filter,watches);
        System.out.println("Gender" + watches.get(0).getGender().getId());
        if(filter.getGender()!=null)
            result = filterGender(filter.getGender(),result);
        if(filter.getType()!=null)
            result = filterType(filter.getType(),result);
        if(filter.getBrand()!=null)
            result = filterBrand(filter.getBrand(),result);
        if(filter.getMechanism()!=null)
            result = filterMechanism(filter.getMechanism(),result);
        if(filter.getCountry()!=null)
            result = filterCountry(filter.getCountry(),result);
        if(filter.getJoin()!=null)
            result = filterJoin(filter.getJoin(),result);
        return result;

    }

    private List<Watch> filterGender(Integer i, List<Watch> watches){
        return watches.stream()
                .filter(e->e.getGender().getId() == Long.parseLong(i.toString()))
                .collect(Collectors.toList());
    }
    private List<Watch> filterType(Integer i, List<Watch> watches){
        return watches.stream()
                .filter(e->e.getType().getId() == Long.parseLong(i.toString()))
                .collect(Collectors.toList());
    }
    private List<Watch> filterBrand(Integer i, List<Watch> watches){
        return watches.stream()
                .filter(e->e.getBrand().getId() == Long.parseLong(i.toString()))
                .collect(Collectors.toList());
    }
    private List<Watch> filterMechanism(Integer i, List<Watch> watches){
        return watches.stream()
                .filter(e->e.getMechanism().getId() == Long.parseLong(i.toString()))
                .collect(Collectors.toList());
    }
    private List<Watch> filterCountry(Integer i, List<Watch> watches){
        return watches.stream()
                .filter(e->e.getCountry().getId() == Long.parseLong(i.toString()))
                .collect(Collectors.toList());
    }
    private List<Watch> filterJoin(Integer i, List<Watch> watches){
        return watches.stream()
                .filter(e->e.getSpecialValue().getId() == Long.parseLong(i.toString()))
                .collect(Collectors.toList());
    }


    private String saveFile(String name, MultipartFile file,String fileName){
        loader.writeFile(file,name,fileName);
        return loader.getServerFilePath();
    }

    private List<Watch> priceFilter(FilterForm filter, List<Watch> watches){
        Double from;
        Double to;
        if(filter.getFromPrice()==null) {
            Optional<Double> min = watches.stream()
                    .map(e -> e.getPrice())
                    .reduce(Double::min);
            from = new Double(min.get());
        }else{
            from = new Double(filter.getFromPrice());
        }
        if(filter.getToPrice()==null){
            Optional<Double> max  =  watches.stream()
                    .map(e -> e.getPrice())
                    .reduce(Double::max);
            to = new Double(max.get());
        }else{
            to = new Double(filter.getToPrice());
        }
        List<Watch> result = watchRepository.findByPriceBetween(from,to);
        return result;
    }

    @Override
    public Integer getElements() {
        pages = (int) watchRepository.count();
        return pages;
    }
}
