package ua.chstu.dao.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.chstu.controller.dto.UserForm;
import ua.chstu.dao.Order;
import ua.chstu.dao.User;
import ua.chstu.dao.UserRole;
import ua.chstu.dao.repository.OrderRepository;
import ua.chstu.dao.repository.UserRepository;
import ua.chstu.dao.repository.UserRoleRepository;
import ua.chstu.dao.services.UserService;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository repository;
    @Autowired
    private UserRoleRepository roleRepository;

    @Autowired
    private OrderRepository orderRepository;
    @Override
    public void save(UserForm u) {
        User user = new User(u.getName(),u.getPassword(),u.getEmail());
        repository.save(user);
        user = repository.findByName(u.getName());
        roleRepository.save(new UserRole(user.getId(),"ROLE_USER"));

    }

    @Override
    public String getRole(String name) {
        return roleRepository.findRoleByUserName(name);
    }

    @Override
    public List<Order> history(String name) {
        return orderRepository.findByUserName(name);
    }


}
