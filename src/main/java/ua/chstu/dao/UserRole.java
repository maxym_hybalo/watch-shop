package ua.chstu.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name="user_role")
public class UserRole extends  BaseEntity{

    @Column(name="user_id")
    private Long userId;

    @Column(name="role")
    private String role;

    public UserRole(Long userId, String role) {
        this.role = role;
        this.userId = userId;
    }
}
