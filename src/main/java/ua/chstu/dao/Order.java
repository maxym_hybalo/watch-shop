package ua.chstu.dao;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_id")
    private Long orderId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id",nullable = false)
    private User user;

    @Column(name="order_date")
    private Date date;

    @Column(name="status")
    private String status; //Select item; Enum;

    @Column(name="price")
    private Double price;

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinTable(name="orders_watch",joinColumns = {
            @JoinColumn(name="order_id",nullable = true, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name="watch_id",updatable = false)})
    private Set<Watch> watches;

    @Override
    public String toString() {
        return "Order{" +
                "user=" + user +
                ", date=" + date +
                ", status='" + status + '\'' +
                ", price=" + price +
                '}';
    }

    public Order() {
    }
}
