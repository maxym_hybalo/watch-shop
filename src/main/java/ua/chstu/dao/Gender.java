package ua.chstu.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name="gender")
public class Gender extends BaseEntity {

    @Column(name="name",nullable = false, unique = true)
    private String name;

    public Gender(String name) {
        this.name = name;
    }

    public Gender() {
    }
}
