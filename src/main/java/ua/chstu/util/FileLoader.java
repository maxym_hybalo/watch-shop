package ua.chstu.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Component
@PropertySource("classpath:configuration.properties")
public class FileLoader {
    //TODO logger
    @Value("${server.watch.file.path}")
    private String path;
    @Value(("${server.watch.classpath}"))
    private String basicPath;
    private String serverFilePath;
    public void writeFile(MultipartFile file, String filename,String clientFileName){
        System.out.println(path);
        String extension = substractExtension(clientFileName);
        serverFilePath = path + File.separator + filename + extension;
        String writePath = basicPath + serverFilePath;
        try {
            FileOutputStream stream = new FileOutputStream(new File(writePath));
            byte [] data = file.getBytes();
            stream.write(data);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String substractExtension(String ex){
        System.out.println(ex);
        return ex.substring(ex.length()-4,ex.length());
    }

    public String getServerFilePath() {
        return serverFilePath;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer(){
        return new PropertySourcesPlaceholderConfigurer();
    }
}
