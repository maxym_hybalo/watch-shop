package ua.chstu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ua.chstu.controller.dto.UserForm;
import ua.chstu.dao.services.UserService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/signup")
    public String signUp(Model model){
        model.addAttribute("user", new UserForm());
        return "signup";

    }

    @PostMapping("/signup")
    public String signed(@ModelAttribute UserForm form){
        service.save(form);
        return "redirect: /watch/";
    }

    @GetMapping("/signin")
    public String sign(){
        return "login";
    }

    @PostMapping("/signin")
    public String signIn(){
        return "redirect: /watch/";
    }

    @GetMapping("/logout")
    public String out(){
        return "profile";
    }

    @PostMapping("/logout")
    public String logout(){
        return "redirect: /signin";
    }

    @GetMapping("/profile")
    public String profile(HttpServletRequest request,Model model){
        model.addAttribute("history", service.history(request.getRemoteUser()));
        return "profile";
    }
//    @GetMapping("/signup/{name}")
//    public String role(@PathVariable String name){
//        return  service.getRole(name);
//    }
}
