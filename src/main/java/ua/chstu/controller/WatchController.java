package ua.chstu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.chstu.controller.dto.FilterForm;
import ua.chstu.controller.dto.WatchForm;
import ua.chstu.dao.Watch;
import ua.chstu.dao.services.DataService;
import ua.chstu.dao.services.WatchService;


@Controller()
@RequestMapping("/watch")
public class WatchController {

    @Autowired
    private DataService service;

    @Autowired
    private WatchService watchService;

    @GetMapping("/new")
    public String form(Model model){
        WatchForm watchForm = new WatchForm();
        model.addAttribute("watchForm", watchForm);
        setSelectToSession(model);
        return "watch_manager";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute WatchForm watchForm,
                         @RequestParam("file") MultipartFile file,
                         @RequestParam("fileName") String fileName){
        System.out.println(fileName);
        watchService.add(watchForm,file,fileName);
        return "redirect: ";
    }

    @GetMapping("/")
    public String window(Pageable pageable,Model model){
        model.addAttribute("filter",new FilterForm());
        model.addAttribute("watches",watchService.get(pageable));
        setSelectToSession(model);

        return "window";
    }


    @GetMapping("/page")
    @ResponseBody
    public Integer pages(){
        return watchService.getElements();
    }

    //to use filters
    //Controller get some params, transfer to service which compute data and give result collection
    //Add come message(tags will be nice)
    @PostMapping("/filter")
    public String filter(@ModelAttribute FilterForm filter,Model model){
        model.addAttribute("filter",new FilterForm());
        setSelectToSession(model);
        model.addAttribute("watches",watchService.filter(filter));
        return "window";
    }
    private void setSelectToSession(Model model){
        model.addAttribute("brands",service.getAll(1));
        model.addAttribute("countries",service.getAll(2));
        model.addAttribute("genders",service.getAll(3));
        model.addAttribute("types",service.getAll(4));
        model.addAttribute("mechanisms",service.getAll(5));
        model.addAttribute("joins",service.getAll(6));
    }
}
