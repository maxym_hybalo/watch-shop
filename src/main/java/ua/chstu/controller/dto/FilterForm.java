package ua.chstu.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilterForm {

    private Integer fromPrice;
    private Integer toPrice;
    private Integer gender;
    private Integer brand;
    private Integer country;
    private Integer mechanism;
    private Integer join;
    private Integer type;

}
