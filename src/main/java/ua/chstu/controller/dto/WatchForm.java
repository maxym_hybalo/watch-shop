package ua.chstu.controller.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class WatchForm {

    private String model;

    private Long brand;

    private Double price;

    private Long country;

    private Long gender;

    private Long type;

    private Long  mechanism;

    private Long join;

}
