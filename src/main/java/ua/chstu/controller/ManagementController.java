package ua.chstu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.chstu.dao.services.DataService;

@Controller
@RequestMapping("/manager")
public class ManagementController {
    @Autowired
    DataService service;
//    @PostMapping("/manager/1")
//    public String add(@RequestParam String name,@RequestParam String path,@RequestParam ){
////        service.add(id,name,path);
//        System.out.println(name + path);
//        return "redirect: /manager";
//    }

    @PostMapping("/manager/{id}")
    public String add(@PathVariable  Integer id,String name,String path,MultipartFile file){
        if(id==1){
            service.setFile(file);
            service.add(id,name,path);
        }else {
        service.add(id,name);
        }
        System.out.println(name);
        return "redirect: ";
    }

    @GetMapping("/manager")
    public String index(){
        return "manage";
    }
}
